# Battleship

A Rails-API to support a battleship-type game. Built to fulfill the requirements [here](https://github.com/ambiata/interview/blob/master/battleship.md).

## Getting Started

### Prerequisites

The application runs on Ruby 2.6.3. If you don't have it, you can install using rvm.

```
rvm install 2.6.3
```

The database is PostgreSQL - if you don't have it already, you can install it using the installer [here](https://www.postgresql.org/download/).

### Installation

First, clone the repository:

```
git clone git@bitbucket.org:ipc103/battleship.git
cd battleship
```

Install the gems:

`bundle install`

Create the database and run the migrations:

```
rails db:create
rails db:migrate
```
You can now start a server and check out the app!

```
rails s
```

### Running the Tests

The test suite is built in RSpec, along with Shoulda matchers for readability. You can run the entire test suite using the `rspec` command, or an individual file using `rspec path/to/file`.

Currently, the app includes just a few sample specs for the models, as well as some sample request specs. Next steps would be to add tests for the remaining available requests, as well as the service and model layer.

## API Description

Our application is a RESTful API that supports the following set of operations for a game of battleship. *Note* - we assume we're using a Token-based Authorization system, such as JWT, to identify the account making the request.

The minimum functionality we want to support:

- Create an empty board.

- Place a ship on the board.

- Create a random board with the ships already placed.

- Make an attacking move, determining if it is a hit or a miss and
  updating the game state.

- Determine the current state of the game, finished (and who won), or in play.

### Creating an Account

Users can create an account via the following request: `post /api/v1/signup` - email, username, and password are required.

### Games

The application supports both READ and CREATE actions for games. Note that, to view a particular game, a player must be associated with the given game, otherwise we return a Not-Authorized response.

Players can create a new game using the following request:

`post /api/v1/games`:

Returns the following response:

```
{
  id: ID of the created game,
  opposing_player_id: ID of the opposing player,
  board_id: ID of the board for the player who created the game,
  opponent_board_id: ID of the board for the opponent,
  status: 'new' - status of the game,
  started_at: datetime reflecting when the game was created,
  ended_at: nil, # Datetime for when the game was marked over
  winner_id: nil # ID of the winning player
}
```

Players can determine the state of a particular game. The following returns game details.

`get /api/v1/games/:id`

Returns:

```
{
  id: ID of the created game,
  opposing_player_id: ID of the opposing player,
  board_id: ID of the board for the player who created the game,
  opponent_board_id: ID of the board for the opponent,
  status: 'new' - status of the game,
  started_at: datetime reflecting when the game was created,
  ended_at: nil, # Datetime for when the game was marked over
  winner_id: nil # ID of the winning player
}
```
### Viewing a Board

To view the details of a particular board:

`get /api/v1/boards/:id`

This request returns a different level of detail, depending on the player's relationship to the board.

For your own board:

```
{
  id: ID of the board,
  x_dimensions: 10, hard coded to describe the shape of the board
  y_dimensions: 10,  hard coded to describe the shape of the board
  active_square_count: 12, how many squares are occupied, but not hit
  ships: ['A1', 'A2', 'A3'...], what squares contain ships
  hits: ['A1', 'A2', 'A3'], which squares have already been hit
  misses: ['J4', 'J9', 'J11'] which squares have been missed
}
```

For an opponent's board:

```
{
  id: 9, ID of the board
  x_dimensions: 10, hard coded to describe the shape of the board
  y_dimensions: 10, hard coded to describe the shape of the board
  active_square_count: 12, how many squares are occupied, but not hit
  hits: ['A1', 'A2', 'A3'], which squares have already been hit
  misses: ['J4', 'J9', 'J11'] which squares have been missed
}
```

For a board where the requesting account is not part of the game, we return a Not Authorized status error message.

### Playing the Game

Playing the game consists of two phases: placing ships onto the board, and attacking squares on the opponent's board.

#### Placing Ships

You can add a ship to your own board via  `post /api/v1/placements`. The following parameters must be required.

```
{
  board_id: ID of the board you're placing a ship onto
  start_row: '1',
  end_row: '5',
  start_column: 'A',
  end_column: 'A',
  direction: 'horizontal'
  ship_type: 'carrier'

}
```

To populate an existing board at random, use the random boards resource. You must provide the id of the board you're trying to randomize.

`post /api/v1/random_board`

#### Attacking

To make an attacking move, use the attacks resource.

`post /api/v1/attacks` The following parameters must be supplied:

```
{
  board_id: ID of the board to attack
  column: Column of the square to attack i.e. 'A'
  row: Row of the square to attack, i.e. '5'
}
```

The response will indicate the result of the attack.

```
{
  board_id: 5,
  row: 'A',
  column: '5',
  result: 'hit',
  game_id: 3
}
```

#### Winning

The status of the board and the game are updated upon each successful attack. The game status is available at any time via `get /api/v1/games/:id`.

## Architecture Overview

The API follows RESTful conventions for updating the different resources of a Battleship game. A client-side application can interact with the game via the endpoints provided. This should allow for flexibility, as specific details of the game flow could change while preserving the endpoints in question.

The app follows a relatively standard MVC pattern for Rails (with JSON serializers used in place of Views). The [Fast JSON Api](https://github.com/Netflix/fast_jsonapi) Gem was included for serialization for its performance and readability.

Service Objects were used in places where multiple models needed to interact with each other, or when the logic was too complex to include in a model, such as populating a board randomly or starting a new game. This helps keep domain logic out of the controller without leading to overly-bloated models.

## Overview of Models

Modeling Battleship is sneakily complex, as there are many different pieces of information which need to be stored, as well as interactions between different pieces of state for the game.

### Account

The account is responsible for storing credentials, such as usernames and passwords. The `bcrypt` gem was included to avoid storing passwords in plain text. This abstraction allows for separating domain-specific logic from general web-application logic. The `Account` attributes are:

- username:string
- password:string (stored as password_digest via BCrypt)
- email:string
- has_one: player

### Player

The player represents a player in one or many games of battleship. The player controls many boards, and many games through those boards. A `Player` has the following attributes:

- display_name:string
- account: belongs_to
- boards: has_many
- games: has_many
- wins: has_many

### Game

Our game represents a single game of Battleship, played between two players. The system is designed to support two player games, but could be expanded to allow support for three or more players. A `Game` has the following attributes:

- started_at:datetime
- ended_at:datetime
- status: enum (created, in_progress, completed)
- winner:belongs_to
- boards: has_many
- players: has_many

### Board

A board represents a 10 by 10 collection of squares. The size of the board is currently not flexible, but could be expanded if we needed to support boards of varying sizes.

Each game has one board per player, representing where that player has stored their ships. Other players involved in the game have the ability to attack the opposing player's board. The `Board` has the following attributes:


- player:belongs_to `Board#player` - a belongs_to association to a player
- game:belongs_to `Board#game` - a belongs_to association to a game
- active_square_count:integer (default 0)

In addition to what is exposed by ActiveRecord, the `Board` has the following public interface:

- `Board.create_for_new_game` - creates a new board with the provided attributes, and creates the associated empty squares
- `Board#place_ship` - given a series of x and y positions, as well as a ship type, updates the status of the associated squares
- `Board#attack` - given a row and column, updates the status of the corresponding square

### Square
A square represents a single location on a board. The square can be empty, occupied by a ship, missed if it's been previously empty and then attacked, or hit if it's been previously occupied and then attacked. The attributes are:

- position_x:string
- position_y:string
- belongs_to :board
- status:enum (empty occupied hit missed)
- occupied_by:string, with a default of "none"

One potential downside to this approach is that we expose a public interface for the `Square`, even though the `Square` is in many ways an implementation detail of the `Board`. One possible solution would be to use a Private Model as outlined in this [blog post](https://kellysutton.com/2019/10/29/taming-large-rails-codebases-with-private-activerecord-models.html) by Kelly Sutton. However, there are downsides to that approach as well, and the tradeoff did not seem worth it at this time.

### Placement

A placement represents a ship being added to a player's board. This allows a user to track the history of when ships were added, as well as isolate the logic regarding adding ships to the board. The attributes are:

- board:belongs_to
- ship_type: enum (patrol, cruiser, submarine, battleship, carrier)
- start_row:string
- end_row:string
- start_column:string
- end_column:string
- direction:string

## Future Plans

Overall, this was a fun exercise and proved to be an interesting challenge. Some possible next steps:

1. Start building the functionality of the existing method stubs
2. Add specs for the remaining features
3. Deploy to EC2 or ElasticBeanstalk and play Battleship!
