class StartGame

  def self.call(*args, &block)
    new(*args, &block).call
  end

  def initialize(initializing_player: )
    @initializing_player = player
    @game = Game.new
    @opponent = find_opponent
  end

  def call
    game.tap { initialize_game }
  end

  private

  attr_reader :initializing_player, :opponent, :game

  def find_opponent
    # Sample a random player to work against
    # Player.find_random_opponent_for(player)
    # This could also assign an AI player if we wanted...
  end

  def initialization_game
    create_boards
    set_started_at
  end

  def create_board_for_initializing_player
    Board.create_for_new_game(game: game, player: initializing_player)
    Board.create_for_new_game(game: game, player: opponent )
  end

  def set_started_at
    game.started_at = Date.current
  end
end
