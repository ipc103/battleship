class RandomBoard

  def call(*args, &block)
    new(*args, &block).call
  end

  def initialize(board: )
    @board = board
  end

  def call
    #NOTE: would definitely use private methods to keep this method short and readable


    # get all of the ship_types using Placement.ship_types
    # For each ship type, given size N, starting with the largest:
      # load all the board's empty squares into a data structure START_SQUARES
      # choose a square at random, and remove from START_SQUARES
      # Choose a direction at random (forward, backward)
      # Choose an orientation at random (horizontal, diagonal)
      # select the next N -1 squares in the direction selected by incrementing row or column
      # if all of those squares are empty
        # create a new placement with the start and end components
        # call placement.add_to_board
      # otherwise
        # remove the start square from START_SQUARES
        # start over

    # Return the updated board when completed...
  end
end
