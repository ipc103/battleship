class Attack

  def call(*args, &block)
    new(*args, &block).call
  end

  def initialize(game:, attacker:, row:, column:)
    @game = game
    @attacker = attacker
    @row = row
    @column = column
  end

  def call
    if valid?
      board = locate_opponent_board
      board.attack(row, column)
      board.save
      game.check_for_win
    end
  end

  private

  def valid?
    # check that the game is in progress, and that the attacker is associated with the game
  end

  def locate_opponent_board
    # Find the game, and return the board for the opposing player
  end
end
