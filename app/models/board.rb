class Board < ApplicationRecord
  belongs_to :game
  belongs_to :player

  has_many :squares, dependent: :destroy

  def self.create_for_new_game(*args)
    board = self.new(*args).tap { |board| board.send(:populate_empty_squares) }
  end

  def place_ship(x_positions, y_positions, ship_type)
    # find all squares matching x and y positions
    # return false unless all are empty
    # otherwise
      # update each square's status to occupied, ship_type to ship type
      # Increment the number of active squares by the number of squares we updated
      # return true
      # this also validates that the X and Y values are present, becuase we have a validation on the square for the range of allowed values
  end

  def attack(row:, column: )
    square = find_square(row: row, column: column)
    result = square.attack
    if result == :hit
      record_hit
    end
  end

  private

  def populate_empty_squares
    # For each row and column, create a new square which will default as empty
    Square.create_all_for(board: self)
  end

  def find_square(row:, column:)
    # locate square based on x and y position
  end

  def record_hit
    # reduce active square count by one
  end
end
