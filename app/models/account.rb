class Account < ApplicationRecord
  has_secure_password
  has_one :player

  validates :username, :email, presence: true, uniqueness: true
end
