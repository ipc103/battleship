class Square < ApplicationRecord
  VALID_X_POSITIONS = ('1'..'10')
  VALID_Y_POSITIONS = ('A'..'J')

  VALID_SHIPS = %w(none patrol cruiser submarine battleship carrier)

  belongs_to :board

  enum status: { empty: 0, occupied: 1, hit: 2, missed: 3 }

  validates :occupied_by, inclusion: { in: VALID_SHIPS }
  validates :position_x, presence: true, inclusion: { in: VALID_X_POSITIONS }
  validates :position_y, presence: true, inclusion: { in: VALID_Y_POSITIONS }
  # TODO: Add validation that board cannot contain duplicate square...

  def self.create_all_for(board:)
    # Iterate over valid x and y positions - create a new square for each position associted with the passed in board
    # Default occupied_by is none, and default status is empty
  end

  def attack
   # If the square is empty,
   # update the square status to missed
   # if it's occupied
   # update the square status to hit
   # otherwise
   # return the same status
 end

 def description
   # For serializing results in the traditional Battleship format, i.e. "B4"
   "#{position_y}#{position_x}"
 end
end
