class Game < ApplicationRecord
  belongs_to :winner, class_name: 'Player', optional: true

  has_many :boards
  has_many :players, through: :boards

  validates :started_on, presence: true

  enum status: {new: 0, in_progress: 1, finished: 2}, _prefix: '_status'

  def start
    # Check if the status is new
    # if so, check if each board has all of the ships placed on it
    # if so, update status to in progress
  end

  def check_for_win
    # check the active_square_count on the boards - if any is zero.
      # update the ended_at to the current time
      # update the status to finished
      # update the winner to be the other player
  end
end
