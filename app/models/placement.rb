class Placement < ApplicationRecord

  SHIP_TYPES = {patrol: 1, cruiser: 2, submarine: 3, battleship: 4, carrier: 5 }
  VALID_DIRECTIONS = %w(horizontal vertical)

  belongs_to :board
  enum ship_type: SHIP_TYPES

  validates :direction, presence: true, incusion: in: VALID_DIRECTIONS
  validate :ship_can_fit_in_provided_space, :ship_already_placed_on_board

  def self.ship_types
    SHIP_TYPES
  end

  def add_to_board
    # Check if the placement is valid
    # If so, call #place_ship_into_squares
    # if that returns true, save the placment
    # After, call game.start to see if the game is ready to begin
  end

  private

  def place_ship_into_squares
    board.place_ship_into_squares(
      x_positions: x_positions,
      y_positions: y_positions,
      ship_type: ship_type
    )
  end

  def x_positions
    (start_row..end_row)
  end

  def y_positions
    (start_column..end_column)
  end

  def ship_can_fit_in_provided_space
    # adds errors to the collection whether or not this move is possible, without considering if the board is free or not
    # horizontal_distance is defined as start_row - end_row
    # vertical_distance is defined as start_column.ord - end_column.ord - converts char to number
    # Check that either vetical_distance or horizontal_distance is 0
    # Check that vertical_distance + horizontal_distance equals the ship size - 1
  end

  def ship_already_placed_on_board
    # check if the board already has a ship type of this placement's type.
    # IF so, this is invalid - add an error
  end
end
