class Player < ApplicationRecord
  belongs_to :account

  has_many :boards
  has_many :games, through: :boards
  has_many :wins, class_name: 'Game', foreign_key: :winner_id

  validates :display_name,  presence: true, uniqueness: true

  def self.find_random_opponent_for(player: )
    # Finds a random opponent for the player - query all ids excluding given player
  end
end
