module Api
  module V1
    class RandomBoardsController < ApplicationController
      before_action :set_board, :authorize_current_player

      def create
        result = RandomBoard.call(board: @board)
        if result.save
          render json: BoardSerializer.new(result).serializable_hash
        else
          render json: {
            errors: "Unable to process this request"
          }, status: :unprocessable_entity
        end
      end

      private

      def set_board
        @board ||= Board.find_by(id: params[:id])
      end

      def authorize_current_player_for_game
        # check that the current player is the player for the given board
      end
    end
  end
end
