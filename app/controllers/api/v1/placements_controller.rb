module Api
  module V1
    class PlacementsController < ApplicationController
      before_action :authorize_placement

      def create
        placement = Placement.new(placement_params)
        if placement.add_to_board
          # Render JSON using the placmeent serializer
        else
          render json: {
            errors: placement.errors.full_messages
          }, status: :unprocessable_entity
        end
      end

      private

      def authorize_placement
        # authorize request, then make sure that the board they're acting on belongs to the player of the account
        # otherwise - respond unauthorized
      end

      def placement_params
        params.require(:placement).permit(:board_id, :ship_type, :start_square, :direction)
      end
    end
  end
end
