module Api
  module V1
    class AttacksController < ApplicationController
      before_action :set_game, :authorize_current_player_for_game

      def create
        result = Attack.call(row: params[:row], column: params[:column], game: game, attacker: current_player )
        # Render JSON indicating the result of the attack, or errors if it was not success
      end

      private

      def set_game
        @game ||= Game.find_by(id: params[:id])
        unless @game
          render json: {
            error: "No game found with id #{params[:id]}"
          }, status: :not_found
        end
      end

      def authorize_current_player_for_game
        # check that the current player is a player in the given game ID
      end
    end
  end
end
