module Api
  module V1
    class BoardsController < ApplicationController
      before_action :authorize_request

      def show
        # Find the board by the ID parameter.
        # If found, render JSON data about the particular Board
        # Using the BoardSerializer, we would conditionally include information about the locations of the current ships
        # else respond with 404
      end
    end
  end
end
