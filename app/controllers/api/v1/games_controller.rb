module Api
  module V1
    class GamesController < ApplicationController
      before_action :authorize_request

      def show
        # Find the game by the ID parameter.
        # If found, render JSON data about the particular Game
        # Use GameSerializer to determine which attributes to render
        # {
        #   id: 3,
        #   opposing_player_id: 7,
        #   board_id: 10,
        #   opponent_board_id: 5,
        #   status: 'new',
        #   started_at: 'Sun, 03 Nov 2019 09:53:41 -0500',
        #   ended_at: nil,
        #   winner_id: nil
        # }
        # else respond with 404
      end

      def create
        game = StartGame.call( initializing_player: current_account.player )
        # Use GameSerializer to respond with the correct game attributes
        # {
        #   id: 3,
        #   opposing_player_id: 7,
        #   board_id: 10,
        #   opponent_board_id: 5,
        #   status: 'new',
        #   started_at: 'Sun, 03 Nov 2019 09:53:41 -0500',
        #   ended_at: nil,
        #   winner_id: nil
        # }
      end
    end
  end
end
