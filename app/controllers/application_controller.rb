class ApplicationController < ActionController::API

  private

  def authorize_request
    # Validate the presence of a valid Authorization token - render 401 status code otherwise
  end
  
  def current_account
    # STUB: This method would decode an Authorization Token to identify the account of the person sending the request.
  end
end
