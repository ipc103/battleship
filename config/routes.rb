Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      post '/attacks', to: 'attacks#create', as: 'attacks'

      get '/boards/:id', to: 'boards#show', as: 'board'

      get '/games/:id', to: 'games#show', as: 'game'
      post '/games', to: 'games#create', as: 'games'

      post '/random_boards', to: 'random_boards#create', as: 'random_boards'

      post '/placements', to: 'placements#create', as: 'placements'

      post '/signup', to: 'accounts#create', as: 'signup'
      post '/tokens', to: 'tokens#create', as: 'tokens'
    end
  end
end
