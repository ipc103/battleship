class CreateGames < ActiveRecord::Migration[6.0]
  def change
    create_table :games do |t|
      t.datetime :started_at
      t.datetime :ended_at
      t.integer  :status, default: 0
      t.belongs_to :winner

      t.timestamps
    end

    add_index :games, :status
  end
end
