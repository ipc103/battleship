class CreatePlacements < ActiveRecord::Migration[6.0]
  def change
    create_table :placements do |t|
      t.belongs_to :board, null: false, foreign_key: true
      t.integer :ship_type
      t.string :start_row
      t.string :end_row
      t.string :start_column
      t.string :end_column
      t.integer :direction

      t.timestamps
    end
  end
end
