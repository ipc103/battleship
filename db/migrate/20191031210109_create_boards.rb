class CreateBoards < ActiveRecord::Migration[6.0]
  def change
    create_table :boards do |t|
      t.belongs_to :game
      t.belongs_to :player
      t.integer :status
      t.integer :active_square_count, default: 0
    end
  end
end
