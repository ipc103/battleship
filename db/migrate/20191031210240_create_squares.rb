class CreateSquares < ActiveRecord::Migration[6.0]
  def change
    create_table :squares do |t|
      t.belongs_to :board
      t.string :position_x
      t.string :position_y
      t.integer :status, default: 0
      t.string :occupied_by, default: 'none'
    end

    add_index :squares, :position_x
    add_index :squares, :position_y
  end
end
