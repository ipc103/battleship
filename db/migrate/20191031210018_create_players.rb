class CreatePlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :players do |t|
      t.string :display_name
      t.belongs_to :account
    end
  end
end
