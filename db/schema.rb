# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_01_191106) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "username"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "boards", force: :cascade do |t|
    t.bigint "game_id"
    t.bigint "player_id"
    t.integer "status"
    t.integer "active_square_count", default: 0
    t.index ["game_id"], name: "index_boards_on_game_id"
    t.index ["player_id"], name: "index_boards_on_player_id"
  end

  create_table "games", force: :cascade do |t|
    t.datetime "started_at"
    t.datetime "ended_at"
    t.integer "status", default: 0
    t.bigint "winner_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["status"], name: "index_games_on_status"
    t.index ["winner_id"], name: "index_games_on_winner_id"
  end

  create_table "placements", force: :cascade do |t|
    t.bigint "board_id", null: false
    t.integer "ship_type"
    t.string "start_row"
    t.string "end_row"
    t.string "start_column"
    t.string "end_column"
    t.integer "direction"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["board_id"], name: "index_placements_on_board_id"
  end

  create_table "players", force: :cascade do |t|
    t.string "display_name"
    t.bigint "account_id"
    t.index ["account_id"], name: "index_players_on_account_id"
  end

  create_table "squares", force: :cascade do |t|
    t.bigint "board_id"
    t.string "position_x"
    t.string "position_y"
    t.integer "status", default: 0
    t.string "occupied_by", default: "none"
    t.index ["board_id"], name: "index_squares_on_board_id"
    t.index ["position_x"], name: "index_squares_on_position_x"
    t.index ["position_y"], name: "index_squares_on_position_y"
  end

  add_foreign_key "placements", "boards"
end
