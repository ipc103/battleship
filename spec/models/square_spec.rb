require 'rails_helper'

RSpec.describe Square, type: :model do
  describe 'associtions' do
    it { should belong_to :board }
  end

  describe 'validations' do
    it { should validate_inclusion_of(:occupied_by).in_array %w(none patrol cruiser submarine battleship carrier)}

    it { should validate_presence_of :position_x }
    it { should validate_inclusion_of(:position_x).in_array ('1'..'10').to_a  }

    it { should validate_presence_of :position_y }
    it { should validate_inclusion_of(:position_y).in_array ('A'..'J').to_a  }
  end

  describe 'status' do
    it { should define_enum_for(:status).with_values([:empty, :occupied, :hit, :missed]) }
  end
end
