require 'rails_helper'

RSpec.describe Game, type: :model do
  it { should belong_to :winner }
  it { should have_many :boards }
  it { should have_many :players }
end
