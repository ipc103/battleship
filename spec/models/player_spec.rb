require 'rails_helper'

RSpec.describe Player, type: :model do
  it { should belong_to :account }
  it { should have_many :boards }
  it { should have_many :games }
  it { should have_many :wins }
end
