require 'rails_helper'

RSpec.describe Board, type: :model do
  it { should belong_to :player }
  it { should belong_to :game }
  it { should have_many :squares }
end
