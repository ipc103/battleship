require 'rails_helper'

RSpec.describe "Games", type: :request do
  describe "POST /games" do

    context "with no authorization header" do
      it "responds with an unauthorized status" do
        # create a game
        # make a request with no authorization
        # Add expectation here that we return an unauthorized status code
      end
    end

    context "with a valid authorization header" do
      it "returns a 200 status code" do
        post api_v1_games_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /games/:id" do
    context "with no authorization header" do
      it "responds with an unauthorized status" do
        # create a game
        # make a request with no authorization
        # Add expectation here that we return an unauthorized status code
      end
    end

    context "with an authorization header, when an account is not associated with a game" do
      it "responds with an unauthorized status" do
        # create a game
        # create an account
        # make the request with an Auth header for the account
        # Add expectation here that we return an unauthorized status code
      end
    end

    context "with an authorization header, when an account is associated with a game" do
      it "responds with the correct game details" do
        # create account and player
        # create game associated with the player
        # make the request with an Auth header for the account
        # add expectation that we respond with the game details we're expecting
      end
    end
  end
end
