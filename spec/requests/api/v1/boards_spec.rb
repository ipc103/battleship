require 'rails_helper'

RSpec.describe "Boards", type: :request do
  describe "GET /boards/:id" do
    context "with no authorization header" do
      it "responds with an unauthorized status" do
        # create a board
        # make a request with no authorization
        # Add expectation here that we return an unauthorized status code
      end
    end

    context "with an authorization header, when an account is not associated with a board" do
      it "responds with an unauthorized status" do
        # create a board
        # create an account
        # make the request with an Auth header for the account
        # Add expectation here that we return an unauthorized status code
      end
    end

    context "with an authorization header, when an account is associated with a board" do
      it "responds with the correct board details" do
        # create account and player
        # create board associated with the player
        # make the request with an Auth header for the account
        # add expectation that we respond with the board details we're expecting
      end
    end
  end
end
