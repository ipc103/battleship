require 'rails_helper'

RSpec.describe "Placements", type: :request do
  describe "POST /placements/:id" do
    context "with no authorization header" do
      it "responds with an unauthorized status" do
        # create a game
        # make a request with no authorization
        # Add expectation here that we return an unauthorized status code
      end
    end

    context "with an authorization header, when the board belongs to the opponent" do
      it "responds with an unauthorized status" do
        # create a game
        # create an account
        # make a request to place a ship onto the other player's board
      end
    end

    context "with an authorization header, when the board belongs to the current player" do
      context "with a valid move" do
        it "creates the placement successfully" do
          # create account and player
          # create game associated with the player
          # make the request with an Auth header for the account
          # add expectation that we respond with the game details we're expecting
        end
      end

      context "when the ship has already been placed" do
        it "does not create the placement and returns an error message" do
          # create a board and create a placement onto it
          # try to create the placement again and test that we get an error back
        end
      end

      context "when the squares aren't in the correct position" do
        # Try a few different invalid moves here...
        it "does not create the placement and returns an error message" do
          # create a board, try to make an invalid move onto the board...
        end
      end
    end
  end
end
